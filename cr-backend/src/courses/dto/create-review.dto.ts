import { IsInt, IsNotEmpty } from "class-validator";
import { ObjectId } from "mongodb";

export class CreateReviewDto {

    @IsNotEmpty()
    comments: string;

    @IsInt()
    score: number;
    
    courseId?: ObjectId;
}
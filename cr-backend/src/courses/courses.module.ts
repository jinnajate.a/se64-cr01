import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";

import { CourseController } from "./courses.controller";
import { CoursesService } from "./courses.service";

import Course from "./course.entity";
import Review from "./review.entity";

@Module({
    imports: [TypeOrmModule.forFeature([Course, Review])],
    controllers: [CourseController],
    providers: [CoursesService]
})
export class CoursesModule {}
import { NUMBER_BINARY_OPERATORS } from "@babel/types";
import React, { useState } from "react";
import { Course, Review } from "../interfaces";
import CoursesService from "../services/CoursesService"

type CourseItemProps = {
    course: Course,
};

const CourseItem = (props: CourseItemProps) => {
    const course: Course = props.course;

    const [reviewsVisible, setReviewsVisible] = useState<boolean>(false);
    const [reviews, setReviews] = useState<Review[]>([]);
    
    const [newReviewComment, setNewReviewComment] = useState<string>('');
    const [newReviewScore, setNewReviewScore] = useState<number>(1);

    const fetchReviews = () => {
        if (course.id) {
            CoursesService.fetchReviews(course.id)
                    .then(reviews => {
                        setReviews(reviews);
                    });
        }
    }

    const handleReviewsVisibleToggle = () => {
        if (!reviewsVisible) {
            fetchReviews();
            setReviewsVisible(true);
        } else {
            setReviewsVisible(false);
        }
    };

    const clearNewReviewForm = () => {
        setNewReviewComment('');
        setNewReviewScore(1);
    };

    const handleNewReviewSaved = () => {
        const newReview: Review = {
            comments: newReviewComment,
            score: newReviewScore
        };
        if(course.id) {
            CoursesService.createReview(newReview, course.id)
            .then(savedNewReview => {
                if(savedNewReview){
                    fetchReviews();
                    clearNewReviewForm();
                }
            })
        }
    }

    const newReviewScoreOption = [1,2,3,4,5];

    return (
        <li className="Course">
            {course.number} - {course.title}
            &nbsp;&nbsp;
            <button onClick={handleReviewsVisibleToggle}>
                {reviewsVisible ? 'hide reviews' : 'show reviews'}
            </button>
            {reviewsVisible &&
                <div> 
                    <ul>
                        {reviews.map(reviews => (
                            <li>{reviews.comments} ({reviews.score})</li>
                        ))}
                        {reviews.length === 0 &&
                            <li>No reviews</li>
                        }
                    </ul>
                    <b> New Review </b><br/>
                    Comments: &nbsp;
                    <input 
                        onChange={(e) => {setNewReviewComment(e.target.value); }}
                        value={newReviewComment}/>
                    &nbsp; Score: &nbsp;
                    <select 
                        onChange={(e) => {setNewReviewScore(parseInt(e.target.value, 10)); }}
                        value={newReviewScore}>
                        {newReviewScoreOption.map( item =>
                            <option value={item}>{item}</option>
                            )}
                    </select>
                    &nbsp;
                    <button onClick={handleNewReviewSaved}> Save </button>
                </div>
            }
        </li>
    );
};

export default CourseItem